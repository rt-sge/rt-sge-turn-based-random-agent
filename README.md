## Turn Based Random Agent

A generic random agent for any turn based strategy game.

Performs random actions.

package at.ac.tuwien.ifs.sge.agent;

import at.ac.tuwien.ifs.sge.core.agent.AbstractTurnBasedGameAgent;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;
import at.ac.tuwien.ifs.sge.core.util.Util;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
    Turn Based Random Agent

    A generic random agent for any turn based strategy game.

    Performs random actions.
 */
public class TurnBasedRandomAgent<G extends TurnBasedGame<A, ?>, A> extends AbstractTurnBasedGameAgent<G, A> {

    /*
        The agent is started as a new process that gets passed three arguments:
            1. the player id of the player it is going to represent
            2. the player name of the player it is going to represent
            3. the game class name of the game it should play. This is only relevant if the agent supports multiple games.

        Since the 'Main-Class' attribute of the JAR file defined in the build.gradle points to this class
        a main method is defined which gets called when the engine starts the agent. The agent is then instantiated
        and started through the start() method, which also starts the agent's communication with the engine server.
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        int playerId = getPlayerIdFromArgs(args);
        String playerName = getPlayerNameFromArgs(args);
        var gameClass = ( Class<? extends TurnBasedGame<Object, Object>> ) getGameClassFromArgs(args);
        var agent = new TurnBasedRandomAgent<>(gameClass, playerId, playerName, -2);
        agent.start();
    }

    private final Random random = new Random();

    /*
        A constructor calling the super constructor is required when extending the abstract agent.

        Since this agent can play any real time game the game class has a generic type.
     */
    public TurnBasedRandomAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        super(gameClass, playerId, playerName, 1, 2, TimeUnit.SECONDS, logLevel);

    }

    /*
        When it is the agents turn it receives a ComputeNextActionEvent together with the new game state
        and the allowed computation time.
        It then calls the computeNextAction(G game, long computationTime, TimeUnit timeUnit) method which
        should return the computed action. The first call should always be the setTimers(computationTime, timeUnit)
        method in order to use the methods 'shouldStopComputationTime', 'nanosLeft' and 'nanosElapsed' of the
        abstract agent implementation. Afterwards the game state in 'game' can be used to determine the
        next action.

        The agent randomly picks a possible action from the game state and returns it.
     */
    @Override
    public A computeNextAction(G game, long computationTime, TimeUnit timeUnit) {
        setTimers(computationTime, timeUnit);

        try {
            Thread.sleep(10);
        } catch (InterruptedException ignored) {}
        return Util.selectRandom(game.getPossibleActions(), random);
    }
}
